package ru.t1.ktubaltseva.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.repository.ISessionRepository;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.ILoggerService;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.api.service.IUserService;
import ru.t1.ktubaltseva.tm.dto.model.SessionDTO;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.service.ConnectionService;
import ru.t1.ktubaltseva.tm.service.LoggerService;
import ru.t1.ktubaltseva.tm.service.PropertyService;
import ru.t1.ktubaltseva.tm.service.UserService;

import java.util.Collection;
import java.util.Collections;

import static ru.t1.ktubaltseva.tm.constant.SessionTestData.*;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    private static final ILoggerService loggerService = new LoggerService();

    @NotNull
    private static final IPropertyService propertyService = new PropertyService(loggerService);

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final SqlSession sqlSession = connectionService.getSqlSession();
    @NotNull
    private final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);

    @NotNull
    private static final IUserService userService = new UserService(propertyService, connectionService);

    @BeforeClass
    @SneakyThrows
    public static void before() {
        userService.add(USER_1);
        userService.add(USER_2);
        sqlSession.commit();
    }

    @AfterClass
    @SneakyThrows
    public static void afterClazz() {
        userService.removeOne(USER_1);
        userService.removeOne(USER_2);
        sqlSession.commit();
        sqlSession.close();
    }

    @After
    @SneakyThrows
    public void after() {
        sqlSession.commit();
        for (@NotNull final SessionDTO session : SESSION_LIST) {
            try {
                repository.removeById(session.getId());
            } catch (@NotNull final Exception e) {

            }
        }
        repository.clearByUserId(USER_1.getId());
        repository.clearByUserId(USER_2.getId());
        sqlSession.commit();
    }

    @Test
    @SneakyThrows
    public void add() {
        @Nullable final SessionDTO sessionToAdd = USER_1_SESSION_1;
        @Nullable final String sessionToAddId = sessionToAdd.getId();

        repository.add((sessionToAdd));
        @Nullable final SessionDTO sessionFindOneById = repository.findOneById(sessionToAddId);
        Assert.assertNotNull(sessionFindOneById);
        Assert.assertEquals(sessionToAdd.getId(), sessionFindOneById.getId());
    }

    @Test
    @SneakyThrows
    public void addByUserId() {
        @Nullable final String userToAddId = USER_1.getId();
        @Nullable final String userNoAddId = USER_2.getId();
        @Nullable final SessionDTO sessionToAddByUser = USER_1_SESSION_1;
        @Nullable final String sessionToAddByUserId = sessionToAddByUser.getId();

        repository.add(sessionToAddByUser);
        @Nullable final SessionDTO sessionFindOneById = repository.findOneById(sessionToAddByUserId);
        Assert.assertNotNull(sessionFindOneById);
        Assert.assertEquals(sessionToAddByUser.getId(), sessionFindOneById.getId());

        @Nullable final SessionDTO sessionFindOneByIdByUserIdToAdd = repository.findOneByIdByUserId(userToAddId, sessionToAddByUserId);
        Assert.assertNotNull(sessionFindOneByIdByUserIdToAdd);
        Assert.assertEquals(sessionToAddByUser.getId(), sessionFindOneByIdByUserIdToAdd.getId());

        @Nullable final SessionDTO sessionFindOneByIdByUserIdNoAdd = repository.findOneByIdByUserId(userNoAddId, sessionToAddByUserId);
        Assert.assertNull(sessionFindOneByIdByUserIdNoAdd);
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @NotNull final SessionDTO sessionExists = USER_1_SESSION_1;
        repository.add(sessionExists);

        @Nullable final SessionDTO sessionFindOneById = repository.findOneById(sessionExists.getId());
        Assert.assertNotNull(sessionFindOneById);
        Assert.assertEquals(sessionExists.getId(), sessionFindOneById.getId());

        @Nullable final SessionDTO sessionFindOneByIdNonExistent = repository.findOneById(NON_EXISTENT_SESSION_ID);
        Assert.assertNull(sessionFindOneByIdNonExistent);
    }

    @Test
    @SneakyThrows
    public void findOneByIdByUserId() {
        @NotNull final SessionDTO sessionExists = USER_1_SESSION_1;
        @NotNull final UserDTO userExists = USER_1;
        repository.add(sessionExists);

        Assert.assertNull(repository.findOneByIdByUserId(userExists.getId(), NON_EXISTENT_SESSION_ID));
        Assert.assertNull(repository.findOneByIdByUserId(NON_EXISTENT_USER_ID, sessionExists.getId()));

        @Nullable final SessionDTO sessionFindOneById = repository.findOneByIdByUserId(userExists.getId(), sessionExists.getId());
        Assert.assertNotNull(sessionFindOneById);
        Assert.assertEquals(sessionExists.getId(), sessionFindOneById.getId());
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final SessionDTO sessionExists = USER_1_SESSION_1;

        repository.clear();
        @NotNull final Collection<SessionDTO> sessionsFindAllEmpty = repository.findAll();
        Assert.assertNotNull(sessionsFindAllEmpty);
        Assert.assertEquals(Collections.emptyList(), sessionsFindAllEmpty);

        repository.add(sessionExists);
        @NotNull final Collection<SessionDTO> sessionsFindAllNoEmpty = repository.findAll();
        Assert.assertNotNull(sessionsFindAllNoEmpty);
    }

    @Test
    @SneakyThrows
    public void findAllByUserId() {
        @NotNull final SessionDTO sessionExists = USER_1_SESSION_1;
        @NotNull final UserDTO userExists = USER_1;

        repository.add(sessionExists);
        @NotNull final Collection<SessionDTO> sessionsFindAllByUserRepNoEmpty = repository.findAllByUserId(userExists.getId());
        Assert.assertNotNull(sessionsFindAllByUserRepNoEmpty);

        @NotNull final Collection<SessionDTO> sessionsFindAllByNonExistentUser = repository.findAllByUserId(NON_EXISTENT_USER_ID);
        Assert.assertNotNull(sessionsFindAllByNonExistentUser);
        Assert.assertEquals(Collections.emptyList(), sessionsFindAllByNonExistentUser);
    }

    @Test
    @SneakyThrows
    public void clearByUserId() {
        @NotNull final UserDTO userToClear = USER_1;
        @NotNull final UserDTO userNoClear = USER_2;
        @NotNull final String userToClearId = userToClear.getId();
        @NotNull final String userNoClearId = userNoClear.getId();
        @NotNull final SessionDTO sessionByUserToClear = USER_1_SESSION_1;
        repository.add(USER_1_SESSION_1);
        @NotNull final SessionDTO sessionByUserNoClear = USER_2_SESSION_1;
        repository.add(USER_2_SESSION_1);
        repository.clearByUserId(userToClearId);

        @Nullable final SessionDTO sessionFindOneByIdToClear = repository.findOneById(sessionByUserToClear.getId());
        Assert.assertNull(sessionFindOneByIdToClear);
        Assert.assertEquals(0, repository.findAllByUserId(userToClearId).size());

        @Nullable final SessionDTO sessionFindOneByIdNoClear = repository.findOneById(sessionByUserNoClear.getId());
        Assert.assertEquals(sessionByUserNoClear.getId(), sessionFindOneByIdNoClear.getId());
        Assert.assertNotEquals(0, repository.findAllByUserId(userNoClearId).size());
    }

    @Test
    @SneakyThrows
    public void removeById() {
        @Nullable final SessionDTO sessionToRemove = USER_1_SESSION_1;
        repository.add((sessionToRemove));

        repository.removeById(sessionToRemove.getId());
        @Nullable final SessionDTO sessionFindOneById = repository.findOneById(sessionToRemove.getId());
        Assert.assertNull(sessionFindOneById);
    }

    @Test
    @SneakyThrows
    public void removeByIdByUserId() {
        @NotNull final UserDTO userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final SessionDTO sessionByUserToRemove = USER_1_SESSION_1;
        repository.add((USER_1_SESSION_1));
        @Nullable final SessionDTO sessionByUserNoRemove = USER_2_SESSION_1;
        repository.add((USER_2_SESSION_1));

        repository.removeByIdByUserId(userToRemoveId, sessionByUserToRemove.getId());
        @Nullable final SessionDTO sessionRemovedFindOneById = repository.findOneById(sessionByUserToRemove.getId());
        Assert.assertNull(sessionRemovedFindOneById);

        repository.removeByIdByUserId(userToRemoveId, sessionByUserNoRemove.getId());
        @Nullable final SessionDTO sessionNoRemovedFindOneById = repository.findOneById(sessionByUserNoRemove.getId());
        Assert.assertNotNull(sessionNoRemovedFindOneById);
        Assert.assertEquals(sessionNoRemovedFindOneById.getId(), sessionByUserNoRemove.getId());
    }

}
