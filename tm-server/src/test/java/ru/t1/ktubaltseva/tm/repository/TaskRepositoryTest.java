package ru.t1.ktubaltseva.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.api.service.*;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.service.*;

import java.util.Collection;
import java.util.Collections;

import static ru.t1.ktubaltseva.tm.constant.TaskRepTestData.*;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @NotNull
    private static final ILoggerService loggerService = new LoggerService();

    @NotNull
    private static final IPropertyService propertyService = new PropertyService(loggerService);

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final SqlSession sqlSession = connectionService.getSqlSession();

    @NotNull
    private final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);

    @NotNull
    private static final IUserService userService = new UserService(propertyService, connectionService);

    @NotNull
    private static final IProjectService projectService = new ProjectService(connectionService);

    @BeforeClass
    @SneakyThrows
    public static void before() {
        userService.add(USER_1);
        userService.add(USER_2);
        sqlSession.commit();
        projectService.add(USER_1_PROJECT_1);
        projectService.add(USER_1_PROJECT_2);
        sqlSession.commit();
    }

    @AfterClass
    @SneakyThrows
    public static void afterClazz() {
        projectService.clear(USER_1.getId());
        projectService.clear(USER_2.getId());
        sqlSession.commit();
        userService.removeOne(USER_1);
        userService.removeOne(USER_2);
        sqlSession.commit();
        sqlSession.close();
    }

    @After
    @SneakyThrows
    public void after() {
        sqlSession.commit();
        for (@NotNull final TaskDTO task : TASK_LIST) {
            try {
                repository.removeById(task.getId());
            } catch (@NotNull final Exception e) {

            }
        }
        repository.clearByUserId(USER_1.getId());
        repository.clearByUserId(USER_2.getId());
        sqlSession.commit();
    }

    @Test
    @SneakyThrows
    public void add() {
        @Nullable final TaskDTO taskToAdd = USER_1_TASK_1;
        @Nullable final String taskToAddId = taskToAdd.getId();
        repository.add((taskToAdd));
        @Nullable final TaskDTO taskFindOneById = repository.findOneById(taskToAddId);
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(taskToAdd.getId(), taskFindOneById.getId());
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @NotNull final TaskDTO taskExists = USER_1_TASK_1;
        repository.add(taskExists);

        @Nullable final TaskDTO taskFindOneById = repository.findOneById(taskExists.getId());
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(taskExists.getId(), taskFindOneById.getId());

        @Nullable final TaskDTO taskFindOneByIdNonExistent = repository.findOneById(NON_EXISTENT_TASK_ID);
        Assert.assertNull(taskFindOneByIdNonExistent);
    }

    @Test
    @SneakyThrows
    public void findOneByIdByUserId() {
        @NotNull final TaskDTO taskExists = USER_1_TASK_1;
        repository.add(taskExists);

        Assert.assertNull(repository.findOneByIdByUserId(taskExists.getUserId(), NON_EXISTENT_TASK_ID));
        Assert.assertNull(repository.findOneByIdByUserId(NON_EXISTENT_USER_ID, taskExists.getId()));

        @Nullable final TaskDTO taskFindOneById = repository.findOneByIdByUserId(taskExists.getUserId(), taskExists.getId());
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(taskExists.getId(), taskFindOneById.getId());
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final TaskDTO taskExists = USER_1_TASK_1;

        repository.add(taskExists);
        @NotNull final Collection<TaskDTO> tasksFindAllNoEmpty = repository.findAll();
        Assert.assertNotNull(tasksFindAllNoEmpty);
    }

    @Test
    @SneakyThrows
    public void findAllByUserId() {
        @NotNull final TaskDTO taskExists = USER_1_TASK_1;

        repository.add(taskExists);
        @NotNull final Collection<TaskDTO> tasksFindAllByUserRepNoEmpty = repository.findAllByUserId(taskExists.getUserId());
        Assert.assertNotNull(tasksFindAllByUserRepNoEmpty);

        @NotNull final Collection<TaskDTO> tasksFindAllByNonExistentUser = repository.findAllByUserId(NON_EXISTENT_USER_ID);
        Assert.assertNotNull(tasksFindAllByNonExistentUser);
        Assert.assertEquals(Collections.emptyList(), tasksFindAllByNonExistentUser);
    }

    @Test
    @SneakyThrows
    public void findAllByProjectId() {
        @NotNull final TaskDTO taskExists = USER_1_TASK_1;
        @NotNull final ProjectDTO projectExists = USER_1_PROJECT_1;

        repository.add(taskExists);
        @NotNull final Collection<TaskDTO> tasksFindAllByUserEmptyProject = repository.findAllByProjectId(taskExists.getUserId(), projectExists.getId());
        Assert.assertNotNull(tasksFindAllByUserEmptyProject);
        Assert.assertEquals(Collections.emptyList(), tasksFindAllByUserEmptyProject);

        taskExists.setProjectId(projectExists.getId());
        @NotNull final Collection<TaskDTO> tasksFindAllByUserBindProject = repository.findAllByProjectId(taskExists.getUserId(), projectExists.getId());
        Assert.assertNotNull(tasksFindAllByUserBindProject);

        @NotNull final Collection<TaskDTO> tasksFindAllByNonExistentUser = repository.findAllByProjectId(NON_EXISTENT_USER_ID, projectExists.getId());
        Assert.assertNotNull(tasksFindAllByNonExistentUser);
        Assert.assertEquals(Collections.emptyList(), tasksFindAllByNonExistentUser);

        @NotNull final Collection<TaskDTO> tasksFindAllByNonExistentProject = repository.findAllByProjectId(taskExists.getUserId(), NON_EXISTENT_PROJECT_ID);
        Assert.assertNotNull(tasksFindAllByNonExistentProject);
        Assert.assertEquals(Collections.emptyList(), tasksFindAllByNonExistentProject);
    }

    @Test
    @SneakyThrows
    public void clearByUserId() {
        @NotNull final UserDTO userToClear = USER_1;
        @NotNull final UserDTO userNoClear = USER_2;
        @NotNull final String userToClearId = userToClear.getId();
        @NotNull final String userNoClearId = userNoClear.getId();
        @NotNull final TaskDTO taskByUserToClear = USER_1_TASK_1;
        repository.add(taskByUserToClear);
        @NotNull final TaskDTO taskByUserNoClear = USER_2_TASK_1;
        repository.add(taskByUserNoClear);
        repository.clearByUserId(userToClearId);

        @Nullable final TaskDTO taskFindOneByIdToClear = repository.findOneById(taskByUserToClear.getId());
        Assert.assertNull(taskFindOneByIdToClear);
        Assert.assertEquals(0, repository.findAllByUserId(userToClearId).size());

        @Nullable final TaskDTO taskFindOneByIdNoClear = repository.findOneById(taskByUserNoClear.getId());
        Assert.assertEquals(taskByUserNoClear.getId(), taskFindOneByIdNoClear.getId());
        Assert.assertNotEquals(0, repository.findAllByUserId(userNoClearId).size());
    }

    @Test
    @SneakyThrows
    public void removeById() {
        @Nullable final TaskDTO taskToRemove = USER_1_TASK_1;
        repository.add((taskToRemove));

        repository.removeById(taskToRemove.getId());
        @Nullable final TaskDTO taskFindOneById = repository.findOneById(taskToRemove.getId());
        Assert.assertNull(taskFindOneById);
    }

    @Test
    @SneakyThrows
    public void removeByIdByUserId() {
        @NotNull final UserDTO userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final TaskDTO taskByUserToRemove = USER_1_TASK_1;
        repository.add((taskByUserToRemove));
        @Nullable final TaskDTO taskByUserNoRemove = USER_2_TASK_1;
        repository.add((taskByUserNoRemove));

        repository.removeByIdByUserId(userToRemoveId, taskByUserToRemove.getId());
        @Nullable final TaskDTO taskRemovedFindOneById = repository.findOneById(taskByUserToRemove.getId());
        Assert.assertNull(taskRemovedFindOneById);

        repository.removeByIdByUserId(userToRemoveId, taskByUserNoRemove.getId());
        @Nullable final TaskDTO taskNoRemovedFindOneById = repository.findOneById(taskByUserNoRemove.getId());
        Assert.assertNotNull(taskNoRemovedFindOneById);
        Assert.assertEquals(taskNoRemovedFindOneById.getId(), taskByUserNoRemove.getId());
    }

    @Test
    @SneakyThrows
    public void removeAllByIdByProjectId() {
        @NotNull final UserDTO userToRemove = USER_1;
        @NotNull final ProjectDTO projectToRemove = USER_1_PROJECT_1;
        @NotNull final ProjectDTO projectNoRemove = USER_1_PROJECT_2;
        @Nullable final TaskDTO taskByProjectToRemove = USER_1_TASK_1;
        @Nullable final TaskDTO taskByProjectNoRemove = USER_1_TASK_2;

        taskByProjectToRemove.setProjectId(projectToRemove.getId());
        taskByProjectNoRemove.setProjectId(projectNoRemove.getId());
        repository.add(taskByProjectToRemove);
        repository.add(taskByProjectNoRemove);
        repository.removeAllByProjectId(userToRemove.getId(), projectToRemove.getId());

        @Nullable final TaskDTO taskRemovedFindOneById = repository.findOneById(taskByProjectToRemove.getId());
        Assert.assertNull(taskRemovedFindOneById);

        @Nullable final TaskDTO taskNoRemovedFindOneById = repository.findOneById(taskByProjectNoRemove.getId());
        Assert.assertNotNull(taskNoRemovedFindOneById);
        Assert.assertEquals(taskNoRemovedFindOneById.getId(), taskByProjectNoRemove.getId());
    }

}
