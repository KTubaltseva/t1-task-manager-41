package ru.t1.ktubaltseva.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.model.ICommand;
import ru.t1.ktubaltseva.tm.command.AbstractCommand;

import java.util.Collection;

public final class ArgumentListCommand extends AbstractSystemCommand {

    @NotNull
    private final String NAME = "argument-list";

    @NotNull
    private final String DESC = "Display argument list.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        @NotNull String ARGUMENT = "-al";
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() {
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommandsWithArgument();
        for (@NotNull final ICommand command : commands) {
            @Nullable final String argument = command.getArgument();
            System.out.println(argument);
        }
    }

}
